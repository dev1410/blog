<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/tasks', 'TaskController@index')->name('tasks');
Route::get('/tasks/add', 'TaskController@create')->name('tasks_add');
Route::get('tasks/{task}', 'TaskController@show')->name('tasks_show');
Route::get('tasks/{task}/edit', 'TaskController@edit')->name('tasks_edit');
Route::delete('tasks/{task}', 'TaskController@destroy')->name('tasks_destroy');
Route::post('tasks', 'TaskController@store')->name('tasks_store');
Route::patch('tasks/{task}/update', 'TaskController@update')->name('tasks_update');
