@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Your Tasks') }}</div>

                <div class="card-body">
                    @foreach($tasks as $task)
                        <ul class="row">
                            <li class="col-md-8">
                                {{ $task->title }}
                                {{ $task->body }}
                            </li>
                        </ul>
                    @endforeach

                    @if(count($tasks) == 0)
                        <p>
                            There's no task
                        </p>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
